from Sky import Sky
from Ship import Ship
from Bullet import Bullet 

import pygame
import random

class Game:
    def __init__(self):
        self.width=600
        self.height=400       
        self.mySky=Sky(self.width, self.height, 50)        
        self.screen= pygame.display.set_mode((self.width, self.height))
        self.clock=pygame.time.Clock()
        self.fps=60
        #imágen  
        self.sprites= pygame.image.load("sprites.png")        
        self.shipsprite= pygame.Surface((64,64)).convert()
        self.shipsprite.blit(self.sprites,(0,0), (252,436, 64,64)) 
        self.myShip=Ship()     
        #imagen Bullet 
        self.myBullet=Bullet()
        self.bulletsprite = pygame.image.load("Bullet.png").convert()
        
    
    def checkKeys(self):  
        keys=pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:self.myShip.direction="RIGHT"
        elif keys[pygame.K_LEFT]:self.myShip.direction="LEFT" 
        elif keys[pygame.K_SPACE]: self.myBullet.condition="BULL"   
        else: self.myShip.direction="STOP"
        
    def run(self):
        pygame.init()
        control=True
        while control:
            for event in pygame.event.get():
                if event.type==pygame.QUIT:
                    pygame.quit()
                    return True
            self.screen.fill((0,0,0))
            
            #Show the Sky
            for star in self.mySky.stars:
                r=random.randint(0,255)
                g=random.randint(0,255)
                b=random.randint(0,255)
                pygame.draw.circle(self.screen,(r,g,b), star, 1)
                
            self.myBullet.shooting()    
            self.checkKeys()           
            self.mySky.move()
            
            x=self.myShip.width
            y=self.myShip.height
           
            self.screen.blit(self.shipsprite, (x/2.3,y/1.2))           
            self.screen.blit(self.bulletsprite,(x/2.2,self.myBullet.y/1.15))
            
            self.myShip.updateCoodinates()
          
            pygame.display.flip()
            self.clock.tick(self.fps)

myGame=Game()
myGame.run()